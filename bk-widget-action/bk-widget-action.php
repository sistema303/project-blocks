<!DOCTYPE html>
<html lang="pt_br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Account</title>

    <meta name="theme-color" content="#250071" />
    <link rel="stylesheet" href="../../../default-theme/comps-elements.css">
    <link rel="stylesheet" href="../../../default-theme/theme-default.css">

    <!-- Css do bloco -->
    <link rel="stylesheet" href="bk-widget-action.css">

    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">


    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.5/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.5/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.5/dist/js/uikit-icons.min.js"></script>




</head>

<div class="uk-grid-small uk-child-width-1-4@l  uk-child-width-1-3@m uk-child-width-1-3@s " uk-grid>
    <div>
        <a class="uk-card   uk-card-default uk-flex uk-box-shadow-medium uk-flex-strech  uk-flex-middle bk-dash__btn">
            <div class="bk-dash__btn--cover " style="background-image: url('https://www.esteticaclube.online/lp/wp-content/uploads/2021/09/dash_button_04.png');"></div>
            <div class="bk-dash__btn--info uk-flex uk-flex-column">
                <div class="bk-dash__btn--label">Quero Agendar</div>
            </div>
            <div class="bk-dash__btn--icon"><i class="las la-calendar-check"></i></div>
        </a>
    </div>
    <div>
        <a class="uk-card   uk-card-default uk-flex uk-box-shadow-medium uk-flex-strech  uk-flex-middle bk-dash__btn">
            <div class="bk-dash__btn--cover " style="background-image: url('https://www.esteticaclube.online/lp/wp-content/uploads/2021/09/dash_button_01.png');"></div>
            <div class="bk-dash__btn--info uk-flex uk-flex-column">
                <div class="bk-dash__btn--label">Retorno</div>
                <div class="bk-dash__btn--sublabel">Próximos 30 dias</div>
                <div class="bk-dash__btn--value">12</div>
            </div>
            <div class="bk-dash__btn--icon"><i class="las la-level-up-alt"></i></div>
        </a>
    </div>
    <div>
        <a class="uk-card   uk-card-default uk-flex uk-box-shadow-medium uk-flex-strech  uk-flex-middle bk-dash__btn">
            <div class="bk-dash__btn--cover " style="background-image: url('https://www.esteticaclube.online/lp/wp-content/uploads/2021/09/dash_button_02.png');"></div>
            <div class="bk-dash__btn--info uk-flex uk-flex-column">
                <div class="bk-dash__btn--label">Histórico</div>
                <div class="bk-dash__btn--sublabel">Ultimos 60 dias</div>
                <div class="bk-dash__btn--value">08</div>
            </div>
            <div class="bk-dash__btn--icon"><i class="las la-history"></i></div>
        </a>
    </div>
    <div>
        <a class="uk-card   uk-card-default uk-flex uk-box-shadow-medium uk-flex-strech  uk-flex-middle bk-dash__btn">
            <div class="bk-dash__btn--cover " style="background-image: url('https://www.esteticaclube.online/lp/wp-content/uploads/2021/09/dash_button_03.png');"></div>
            <div class="bk-dash__btn--info uk-flex uk-flex-column">
                <div class="bk-dash__btn--label">Indicação</div>
                <div class="bk-dash__btn--sublabel">Últimos 30 dias</div>
                <div class="bk-dash__btn--value">14</div>
            </div>
            <div class="bk-dash__btn--icon"><i class="las la-share-alt"></i></div>
        </a>
    </div>
</div>

</html>